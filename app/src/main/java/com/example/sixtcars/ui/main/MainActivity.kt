package com.example.sixtcars.ui.main

import android.Manifest
import android.content.Intent
import android.content.IntentSender
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.sixtcars.BR
import com.example.sixtcars.R
import com.example.sixtcars.data.entity.Car
import com.example.sixtcars.databinding.ActivityMainBinding
import com.example.sixtcars.ui.base.BaseActivity
import com.example.sixtcars.ui.dialog.CarListDialog
import com.example.sixtcars.utils.NavigationUtil
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.tbruyelle.rxpermissions3.RxPermissions
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@RequiresApi(Build.VERSION_CODES.N)
class MainActivity : BaseActivity<ActivityMainBinding, CarsViewModel>() {

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mGoogleMap: GoogleMap
    private var cars: ArrayList<Car> = arrayListOf()
    private val REQUEST_CHECK_SETTINGS = 1080


    override fun getBindingVariable(): Pair<Int, Any?> {
        return Pair(BR._all, mViewModel)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun setViewModel() {
        mViewModel = ViewModelProvider(this, viewModelFactory).get(CarsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initUI(savedInstanceState)
        getPermission()
    }

    /** Initialize UI */
    private fun initUI(savedInstanceState: Bundle?) {
        mapView.onCreate(savedInstanceState)
        mapView.onResume()

        MapsInitializer.initialize(this)

        mapView.getMapAsync { gMap ->
            mGoogleMap = gMap
            getCars()
        }

        btnShowList.isEnabled = false
        btnShowList.setOnClickListener {
            NavigationUtil.showDialog(CarListDialog.newInstance(cars), supportFragmentManager, true)
        }
    }

    /** Determine if GPS permission is granted */
    private fun getPermission() {
        RxPermissions(this)
            .request(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            .subscribe { granted ->
                if (granted) {
                    displayLocationSettingsRequest()
                } else {
                    AlertDialog.Builder(this)
                        .setMessage("Please enable GPS Location from app settings")
                        .show()
                }
            }
    }

    /** Display Enable Location dialog */
    private fun displayLocationSettingsRequest() {
        val locationRequest = LocationRequest.create().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            //
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    /** Call api and get cars */
    private fun getCars() {
        mViewModel?.getCars()?.observe(this, {
            when {
                it.isLoading -> {
                    loading.visibility = View.VISIBLE
                }
                it.isSuccess -> {
                    loading.visibility = View.GONE
                    cars = it?.data as ArrayList<Car>
                    setCarsOnMap(it.data)
                    btnShowList.isEnabled = true
                }
                it.isError -> {
                    loading.visibility = View.GONE
                    Log.d(
                        TAG,
                        "error->${it.throwable?.let { it1 -> mViewModel?.errorHandler(it1) }}"
                    )
                }
            }
        })
    }

    /** Set cars on the map */
    private fun setCarsOnMap(data: MutableList<Car>?) {
        if (mViewModel?.isEmptyList(data) == true) {
            Log.d(TAG, "Cars list is empty!!!")
        } else {
            var lat = 0.0
            var lng = 0.0
            data?.stream()?.forEach { car ->
                lat += car.latitude
                lng += car.longitude
                Glide.with(this)
                    .asBitmap()
                    .load(car.carImageUrl)
                    .into(object : CustomTarget<Bitmap>(120, 120) {
                        override fun onResourceReady(
                            bitmap: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            mGoogleMap.addMarker(
                                MarkerOptions()
                                    .position(LatLng(car.latitude, car.longitude))
                                    .title("Driver: ${car.name}")
                                    .snippet("License Plate: ${car.licensePlate}")
                                    .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                            )
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            mGoogleMap.addMarker(
                                MarkerOptions()
                                    .position(LatLng(car.latitude, car.longitude))
                                    .title("Driver: ${car.name}")
                                    .snippet("License Plate: ${car.licensePlate}")
                                    .icon(BitmapDescriptorFactory.defaultMarker())
                            )
                        }
                    })

                // Set camera to center of markers
                val count = data.size
                val center = LatLng(lat / count, lng / count)
                val cameraPosition = CameraPosition.Builder()
                    .target(center)
                    .zoom(12f)
                    .tilt(30f)
                    .build()
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                // Do something
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }
}